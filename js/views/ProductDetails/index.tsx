import React, { FunctionComponent } from 'react';
import { WebView } from 'react-native-webview';
import { useNavigation } from 'react-navigation-hooks'


const ProductDetails: FunctionComponent = () => {
  const { getParam } = useNavigation();

  return (
    <WebView
      source={{ uri: getParam('itemUrl') }}
    />

  );
};

export default ProductDetails;
