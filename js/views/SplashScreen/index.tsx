import React, { useEffect, FunctionComponent } from 'react';
import { Image } from 'react-native';
import { Colors } from '../../styles/base';
import { SafeAreaCenteredContainer } from '../../styles/components';

interface Props {
  navigation: any
}

const SplashScreen: FunctionComponent<Props> = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      conditionalRouting();
    }, 2000);
  });

  async function conditionalRouting() {
    navigation.navigate('ProductsList');
  };

  return (
    <SafeAreaCenteredContainer bgColor={Colors.orange}>
      <Image
        source={require('../../../assets/images/logo.png')}
        resizeMode="center"
      />
    </SafeAreaCenteredContainer>
  );
};

export default SplashScreen;
