import React, { FunctionComponent, useContext } from 'react';
import { CenteredContainer } from '../../styles/components';
import SearchInfo from './components/searchInfo';
import ProductsGrid from './components/productsGrid';


const ProductsList: FunctionComponent = () => {
  return (
    <CenteredContainer>
      <SearchInfo />
      <ProductsGrid/>
    </CenteredContainer>
  );
};

export default ProductsList;
