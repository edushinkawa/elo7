import { createSwitchNavigator } from "react-navigation";

import Routes from "./routes";

const { splashStack, productStack } = Routes;

const getRoutesStack = createSwitchNavigator(
  {
    SplashStack: splashStack,
    ProductStack: productStack
  },
  {
    initialRouteName: "SplashStack",
  }
);

export default getRoutesStack;
