import React from 'react';
import { StyleSheet } from 'react-native'

import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from "../views/SplashScreen";
import ProductsList from "../views/ProductsList";

import Header from '../components/header'
import { Colors } from '../styles/base';
import { Appbar } from 'react-native-paper';
import ProductDetails from '../views/ProductDetails';

const splashStack = createStackNavigator({
  Splash: {
    screen: SplashScreen,
    navigationOptions: {
      header: null
    }
  }
});

const productStack = createStackNavigator({
  ProductsList: {
    screen: ProductsList,
    navigationOptions: () => ({
      headerTitle: (
        <Header />
      ),
      headerStyle: styles.header,
      headerLeft: (
        <Appbar.BackAction onPress={this._goBack} color={Colors.white} />
      ),
      headerRight: (
        <Appbar.Action icon="microphone" onPress={this._handleMore} color={Colors.white} />)
    }),
  },
  ProductDetails: {
    screen: ProductDetails,
    navigationOptions: () => ({
      headerStyle: styles.header,
      headerTintColor: Colors.white,
    }),
  }
});

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.pink,
    height: 60,
    elevation: 0,
    alignContent: 'center',
  }
});


const Routes = {
  splashStack,
  productStack
};

export default Routes;
