import axios from 'axios';
import {BASE_URL} from '../utils/constans';

export class BackendAPI {
  public async getProductsList(callback, searchParam: string = '') {
    const endpoint = this.createEndpoint();
    const params = {params: {q: searchParam}};
    try {
      const result = await endpoint.get(BASE_URL, params);
      callback(null, result.data);
    } catch (error) {
      console.log(`Error fecthing data: ${error}`);
      callback(error);
    }
  }

  private createEndpoint() {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };

    return axios.create({
      headers,
      timeout: 15000,
    });
  }
}
