import React from 'react';
import { View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { ContextProvider } from './provider';
import { Provider as PaperProvider } from 'react-native-paper';

import getRoutesStack from './routes/routesStack';

const App: React.FC = () => {
  const Main = createAppContainer(getRoutesStack);
  return (
    <ContextProvider>
      <PaperProvider>
        <View style={{ flex: 1 }}>
          <Main />
        </View>
      </PaperProvider>
    </ContextProvider>
  );
};

export default App;
