import React, {
  ReactElement,
  ReactNode,
  createContext,
  useState,
  useEffect,
} from 'react';
import {BackendAPI} from '../services/api';
import {IData, IContextProviderProps, IProduct} from '../interfaces';

const defaultData: IData = {
  isLoading: false,
  setIsLoading: (): void => {},
  headerInput: '',
  setHeaderInput: (): void => {},
  products: [],
  getList: (): void => {},
};

export const ContextProvider = (props: IContextProviderProps): ReactElement => {
  const apiService = new BackendAPI();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [headerInput, setHeaderInput] = useState<string>('');
  const [products, setProducts] = useState<Partial<IProduct[]>>();

  useEffect(() => {
    setIsLoading(true);
    apiService.getProductsList(onGetProductsList);
  }, []);

  function onGetProductsList(error, data: IProduct[]) {
    setProducts(data);
    setIsLoading(false);
  }

  function getList(param: string) {
    setIsLoading(true);
    apiService.getProductsList(onGetProductsList, param);
  }

  return (
    <DataContext.Provider
      value={{
        isLoading,
        setIsLoading,
        headerInput,
        setHeaderInput,
        products,
        getList,
      }}>
      {props.children}
    </DataContext.Provider>
  );
};

export const DataContext = createContext(defaultData);
