export default mockData = [
  {
    price: {
      current: 'R$ 206,60',
      installment: '12x R$ 17,22 sem juros',
    },
    picture:
      'https://img.elo7.com.br/product/main/134E8A7/kiy-chocolate-baby-poster.jpg',
    title: 'Kiy Chocolate Baby (com moldura)',
    id: '7B8591',
    _link: 'https://www.elo7.com.br/kiy-chocolate-baby-com-moldura/dp/7B8591',
  },
  {
    price: {
      current: 'R$ 331,70',
      installment: '12x R$ 27,64 sem juros',
    },
    picture:
      'https://img.elo7.com.br/product/main/12E09FB/girafa-amigurumi-crianca.jpg',
    title: 'Girafa Amigurumi',
    id: '74FEEA',
    _link: 'https://www.elo7.com.br/girafa-amigurumi/dp/74FEEA',
  },
];
