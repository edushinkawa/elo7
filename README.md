# elo7 Teste

O teste foi feito seguindo as diretrizes do link https://gist.github.com/elo7-developer/22ffef7542c753922ff5a93b306c0ef3

## Como rodar o projeto:

### Android
- `npm install`
- `react-native link`
- `react-native run-android`

### iOS
- `npm install`
- `pod install` na pasta ios
- `react-native link`
- `react-native run-ios`

## O que foi feito:

- tela com o grid de itens originários da API mock sugerida no link <br />
- o app é iniciado com os itens do mock
- a barra de procura está funcional usando a url param ?q=algumaCoisa
- o usuário ao click em algum item do grid é levado para um webview com os detalhes do items

## Como foi feito:

- foi usado react-native v0.61 para geração das telas nativas do app
- para rápida inicialização foi usado o um template já com as configs do Typescript e parte da estrtura de um outro template meu chamado `Shin-Template`
- react-native-paper para o componente do searchbar
- como mencionado todo o projeto é baseado em TS e com ampla cobertura de tipagem
- foi usado styled-components para estilização e criação de components reutilizáveis na medida do possível
- o gerenciamento de estado foi feito com Context API, que atendeu aos própositos simples da aplicação
- para navegação foi utilizado o React Navigation que também foi utilizado como transportador de props entre telas e também para criação do header
- a estrutura base do projeto se encontra dentro da pasta /js onde a estrutura tenta ao máximo centralizar informações e evitar repetição de código sem necessidade

### O que eu vou ficar devendo:

- testes unitários e end-to-end por falta de tempo hábil
- o que eu testaria: comportamento dos components ao receberem as props provindas de um Context Mocado
- testes e2e com detox simulando o fluxo do usuário entre as telas
