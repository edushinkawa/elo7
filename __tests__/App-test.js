/**
 * @format
 */

import 'react-native';
import React, {useContext} from 'react';

import {
  Spinner,
  Product,
  SearchInfo,
} from '../js/views/ProductsList/components/';
import {
  render,
  waitForElement,
} from 'react-native-testing-library';

import renderer from 'react-test-renderer';

import mockData from '../mocks/mock';
import {renderHook, act} from '@testing-library/react-hooks';
import {DataContext} from '../js/provider';

import '../setupTests';

describe('Product', () => {
  const tree = renderer.create(<Product {...mockData[0]} />).toJSON();
  it('Renders view', () => {
    expect(tree.type).toEqual('View');
  });

  it('Renders Image -> Text -> Text -> Text sequence', () => {
    expect(tree.children[0].type).toEqual('Image');
    expect(tree.children[1].type).toEqual('Text');
    expect(tree.children[2].type).toEqual('Text');
    expect(tree.children[3].type).toEqual('Text');
  });

  it('Renders Correct Texts in specific tags', () => {
    expect(tree.children[1].children).toContain(
      'Kiy Chocolate Baby (com moldura)',
    );
    expect(tree.children[2].children).toContain('R$ 206,60');
    expect(tree.children[3].children).toContain('12x R$ 17,22 sem juros');
  });

  it('Renders Correct Styles in specific tags', () => {
    const tagStyle = tree.children[1].props.style[0].color;
    const tagStyle2 = tree.children[2].props.style[0].color;
    const tagStyle3 = tree.children[3].props.style[0].color;
    expect(tagStyle).toEqual('#00A2A1');
    expect(tagStyle2).toEqual('#F96600');
    expect(tagStyle3).toEqual('#A0A0A4');
  });
});

describe('ProductList', () => {
  it('Renders the <Spinner/> ActivityIndicator', () => {
    const tree = renderer.create(<Spinner />).toJSON();
    expect(tree.children[0].type).toBe('ActivityIndicator');
  });
});

describe('SearchInfo', () => {
  it('Renders the <SearchInfo/> component', () => {
    const tree = renderer.create(<SearchInfo />).toJSON();
    expect(tree.children[0].type).toBe('View');
  });
});

describe('SearchInfo', () => {
  const {result} = renderHook(() => useContext(DataContext));

  test('should display 1 products found', async () => {
    act(() => {
      result.current.products = mockData;
    });
    const wrapper = render(<SearchInfo />);
    await waitForElement(() => wrapper.getByText('2 produtos encontrados'));
  });
  
  test('should display 2 products found', async () => {
    const arrWithOneProduct = mockData.filter((item, index) => index === 0);
    act(() => {
      result.current.products = arrWithOneProduct;
    });
    const wrapper = render(<SearchInfo />);

    await waitForElement(() => wrapper.getByText('1 produtos encontrados'));
  });
})
