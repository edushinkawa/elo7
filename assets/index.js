// Images
import logo from './images/logo.png';
import amex from './images/stp_card_amex.png';
import diners from './images/stp_card_diners.png';
import discover from './images/stp_card_discover.png';
import jcb from './images/stp_card_jcb.png';
import mastercard from './images/stp_card_mastercard.png';
import visa from './images/stp_card_visa.png';
import close from './images/close.png';
import logout from './images/logout.png';
import creditcard from './images/creditcard.png';
import wallet from './images/wallet.png';
import home from './images/home.png';

export { logo, amex, diners, discover, jcb, mastercard, visa, close, logout, creditcard, wallet, home };
